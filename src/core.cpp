#include <iostream>
#include <Rcpp.h>
#include <Tuple.hpp>
#include <FFNN.hpp>
#include <FLRNN.hpp>

using Rcpp::NumericMatrix; using spann::FFNN;   using spann::ANN;
using Rcpp::NumericVector; using Rcpp::List;    using Rcpp::XPtr;
using spann::to_node;      using spann::Neuron; using spann::FLRNN;
using std::string;         using Rcpp::as;      using std::vector;
using Rcpp::RawVector;     using std::tuple;    using spann::AdjacencyList;

#define AS_NUMVEC as<NumericVector>

// [[Rcpp::export]]
SEXP CreateFFNN(int input, int layers, int hidden, int output)
{
  return XPtr<FFNN>(new FFNN(input, layers, hidden, output));
}

// [[Rcpp::export]]
SEXP CreateFLRNN(int input, int hidden, int output, int bias)
{
  return XPtr<FLRNN>(new FLRNN(input, hidden, output, bias));
}

// [[Rcpp::export]]
void SetWeightsFFNN(vector<double> weights, SEXP ann) { XPtr<FFNN>(ann)->LoadWeights(weights); }

// [[Rcpp::export]]
void SetWeightsFLRNN(vector<double> weights, vector<int> synapses, vector<int> end_index, vector<int> af, SEXP ann)
{
  XPtr<FLRNN>(ann)->LoadWeights(weights, synapses, end_index, af);
}

// [[Rcpp::export]]
void SetAdjacencyList(NumericMatrix weights, vector<int> af, SEXP ann)
{
  AdjacencyList data;

  for(int i = 0; i < weights.nrow(); ++i)
  {
    data.push_back(std::make_tuple(vector<tuple<double, int>>(), af[i]));
    for(int j = 0; j < weights.ncol(); ++j)
    {
      if(weights(i, j) == 0) continue;
      std::get<0>(data[i]).push_back({weights(i, j), j});
    }
  }

  XPtr<FLRNN>(ann)->LoadWeights(data);
}

// [[Rcpp::export]]
void PrintAdjacencyList(SEXP ann)
{
  spann::FLRNN* ann_ref = XPtr<spann::FLRNN>(ann);
  vector<int> result_vector;

  for(int i = 0; i < ann_ref->al.size(); i++)
  {
    std::cout << "i: "  << i << " | ";
    for(int j = 0; j < std::get<0>(ann_ref->al[i]).size(); j++)
      std::cout << "j: "  << std::get<1>(std::get<0>(ann_ref->al[i])[j]) << "-" <<  std::get<0>(std::get<0>(ann_ref->al[i])[j]) << " | ";
    std::cout << std::endl;
  }
}

// [[Rcpp::export]]
vector<double> EvaluateInput(SEXP ann, vector<double> input_data, RawVector input_test)
{
  auto temp = vector<bool>(input_test.begin(), input_test.end());

  if(input_data.size() != input_test.size())
    return vector<double>({0});
  return XPtr<ANN>(ann)->GetOutput(input_data, temp);
}

// [[Rcpp::export]]
vector<double> EvaluateInputFFNN(SEXP ann, vector<double> input_data)
{
  return XPtr<ANN>(ann)->GetOutput(input_data);
}

// [[Rcpp::export]]
void ResetState(SEXP ann) { XPtr<ANN>(ann)->ResetState(); }
